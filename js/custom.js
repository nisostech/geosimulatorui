/* back button */
$('.back-button').click(function (e) {
    history.back();
});


/* preview icon */
function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadIcon").files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("uploadPreview").src = oFREvent.target.result;
    };
}

/* Open App details page */
$(".appDetails").click(function () {
    var href = $(this).attr("data-href");
    window.location.href = href;
});

/* Change password checkbox */
$("#change_pass").click(function() {
    $("#old_pass").attr("disabled", !this.checked);
    $("#new_pass").attr("disabled", !this.checked);
    $("#confirm_pass").attr("disabled", !this.checked);
});